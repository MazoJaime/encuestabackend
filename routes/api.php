<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('singup', 'AuthApiController@register');

Route::post('encuesta', 'EncuestaController@index');
Route::post('encuesta/view', 'EncuestaController@view');
Route::post('encuesta/responder', 'EncuestaController@save');

Route::post('login', 'AuthApiController@login');

Route::post('data/roles', 'DataController@roles');

Route::post('data/encuestas/list', 'DataController@encuestaslist');

Route::post('encuestas/store', 'EncuestaController@store');


Route::post('preguntas/index', 'PreguntaController@index');
Route::post('pregunta/store', 'PreguntaController@store');
Route::post('encuesta/edit', 'EncuestaController@edit');
Route::post('encuesta/edit/{id}', 'EncuestaController@editstore');
Route::post('reporte', 'ReporteController@index');





