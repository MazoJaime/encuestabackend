<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class RespuestaPreguntaEncuesta extends Model
{
    protected $table = 'respuesta_pregunta_encuesta';
    protected $fillable = [
        'respuesta_encuesta_id',
        'pregunta_id',
        'valor'
    ];
}
