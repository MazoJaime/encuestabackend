<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\models\Role;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute(string $password): void
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function role(): hasOne
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    /**
     * @param $roles array
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return null !== $this->role()->whereIn('role', $roles)->first();
    }

    /**
     * Function that returns a boolen to know if
     * @param $role string
     * @return bool
     */
    public function hasRole($role)
    {
        return null !== $this->role()->where('role', $role)->first();
    }

    /**
     *  Function that returns a boolean, used to know if user has a role in a list or a desired role
     * @param $role Mixed string| Array
     * @return bool
     */
    public function autorizeRoles($role)
    {
        if (is_array($role)) {
            return $this->hasAnyRole($role) || false;
        } elseif (is_string($role)) {
            return $this->hasRole($role) || false;
        } else {
            return false;
        }

    }

}
