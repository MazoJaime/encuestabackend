<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class RespuestaEncuesta extends Model
{
    protected $table = 'respuesta_encuesta';

    protected $fillable = [
        'nombre',
        'rut',
        'email',
        'telefono',
        'comentario',
        'encuesta_id'
    ];

    public function detallerespuesta()
    {
        return $this->hasMany(RespuestaPreguntaEncuesta::class, 'respuesta_encuesta_id', 'id');
    }

    public function encuesta()
    {
        return $this->belongsTo(Encuesta::class, 'encuesta_id', 'id');
    }
}
