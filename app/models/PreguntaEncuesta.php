<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class PreguntaEncuesta extends Model
{
    protected $table = 'pregunta_encuesta';
    protected $fillable = [
        'pregunta_id',
        'encuesta_id',
        'orden'
    ];
    public $timestamps = false;
}
