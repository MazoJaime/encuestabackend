<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;


class Encuesta extends Model
{
    protected $table = 'encuestas';
    protected $fillable = ['nombre'];

    public function preguntas(): BelongsToMany
    {
        return $this->belongsToMany(Pregunta::class, 'pregunta_encuesta', 'encuesta_id', 'pregunta_id', 'id');
    }
}
