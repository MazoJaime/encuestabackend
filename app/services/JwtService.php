<?php


namespace App\services;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class JwtService
{

    public $manager;
    //todo: key mover a .env
    public $key = 'uiasuiyh21na88OKDASPKoijGYS7IOJISsalcanOlapJASBsancaG7ah6AbsLPQBasiuashuan1JSU8o28has72jajs8hday12asldsaoih820ajhdjd219hdh28hdashd8h2hasoihudsgayusdt32iygayufgashjghgfayudsfghshjvkcasicgoifaehwñhduihgaosdiugfaweflglysdgysdgfyucxjnas672HGFAFJDGJHhjgadfsdfe94';

    /**
     * Recibe un usuario y genera token de acceso o retorna el usuario decodificado
     *
     * @param string $email
     * @param string $password
     * @param null $getHash
     * @return false|string
     */
    public function singup(string $email, string $password, $getHash = null)
    {
        $data = [
            'status' => 'error',
            'msg' => 'Error al intentar ingresar'
        ];

        $singup = false;

        try {
            $user = User::where('email', '=', $email)->firstOrFail();
        } catch (ModelNotFoundException $exception) {
            $data['msg'] = 'Email malo o password incorrectos';
            return json_encode($data);
        }
        if ($user instanceof User && is_object($user)) {
            $singup = true;
        }

        if (password_verify($password, $user->password)) {
            if (true == $singup) {
                //generar token JWT

                $token = [
                    "sub" => $user->id,
                    "email" => $user->email,
                    "name" => $user->name,
                    "role" => $user->role_id,
                    "iat" => time(),
                    "exp" => time() + (7 * 24 * 60 * 60), //7 dias * 24 horas *60 minutos * 60segundos
                    'nbf' => time(),
                    'iss' => env('APP_URL', 'http://localhost')
                ];

                $jwt = JWT::encode($token, $this->key, 'HS256');
                $decoded = JWT::decode($jwt, $this->key, ['HS256']);

                if ($getHash == null) {
                    $data = $jwt;
                } else {
                    $data = $decoded;
                }
            }
        }

        return json_encode($data);
    }

    /**
     * si identity es null retona boleano, en caso contrario retornara el objeto dentro del token
     *
     * @param $jwt
     * @param null $getIdentity
     * @return bool|object
     */
    public function checktoken($jwt, $getIdentity = null)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, array('HS256'));
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }
        if (isset($decoded) && is_object($decoded)) {
            $auth = true;
        } else {
            $auth = false;
        }
        if ($getIdentity === null) {
            return $auth;
        } else if ($getIdentity === true) {
            return $decoded;
        }
    }


}