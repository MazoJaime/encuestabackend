<?php

namespace App\Http\Controllers;

use App\models\Pregunta;
use App\Models\User;
use Illuminate\Http\Request;
use App\services\JwtService;
use Illuminate\Support\Facades\Validator;

class PreguntaController extends Controller
{

    public function index(Request $request)
    {
        $data = ['status' => 'error', 'msg' => 'Send Data via POST'];
        try {
            $token = $request->post('authorization');
        } catch (\Exception $e) {
            $data = ['status' => 'error', 'msg' => 'Send All required data via POST'];
        }

        if ($token !== null) {

            $jwt_auth = new JwtService();
            /**
             * aparte de validar que posea un token, vereficamos que el usuario sea admin para acceder a esta area
             */

            if ($jwt_auth->checktoken($token)) {
                $identity = $jwt_auth->checktoken($token, true);
                $user = User::find($identity->sub);

                if ($user->hasRole('admin')) {

                    $data = ['status' => 'success', 'msg' => Pregunta::all()];

                } else {
                    $data = ['status' => 'error', 'msg' => 'No tienes los privilegios para acceder a esta area'];
                }

            } else {

                $data = ['status' => 'error', 'msg' => 'No tienes permisos para acceder a esta area '];

            }

        }

        return $data;
    }

    public function store(Request $request)
    {
        $respuesta = ['status' => 'error', 'msg' => 'Ha ocurrido un problema, intenta nuevamente '];

        /**
         * validamos recibir info requerida del formulario
         */

        $data = json_decode($request->post('pregunta'), true);

        $reglas = ['texto' => 'required|string|max:150'];

        $mensajes = [
            'texto.required' => 'La pregunta debe no puede estar vacia',
            'texto.string' => 'La pregunta debe ser texto',
            'texto.max' => 'el largo maximo de la pregunta es :max caracteres'
        ];

        $validator = Validator::make($data, $reglas, $mensajes);

        /**
         * si pasa valicacion validamos que tenga permisos
         */
        if ($validator->passes()) {

            $jwt_auth = new JwtService();

            $token = $request->post('authorization', null);

            $identity = $jwt_auth->checktoken($token, true);
            try {
                $user = User::find($identity->sub);
            } catch (\Exception $e) {

            }

            if ( $jwt_auth->checktoken($token) && $user->autorizeRoles('admin')) {

                $encuesta = Pregunta::create([
                    'pregunta' => $data['texto']
                ]);

                $respuesta = ['status' => 'success', 'msg' => "La pregunta se creo correctamente"];

            }

        } else {

            $respuesta = ['status' => 'error', 'msg' => $validator->errors()->first()];
        }

        return $respuesta;

    }
}
