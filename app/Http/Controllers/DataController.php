<?php

namespace App\Http\Controllers;

use App\Models\Encuesta;
use App\models\Role;
use Illuminate\Http\Request;
use App\services\JwtService;

class DataController extends Controller
{

    public function roles(Request $request)
    {
        $helper = New JwtService();

        $token = $request->post('authorization', null);
        $helper->checktoken($token);
        if ($token === null) {
            return $data = [
                'status' => 403,
                'data' => 'No autorizado'
            ];
        }

        $roles = Role::all(['id', 'nombre']);

        return $roles;

    }

    public function encuestaslist(Request $request)
    {
        $helper = New JwtService();

        $token = $request->post('authorization', null);
        $helper->checktoken($token);
        if ($token === null) {
            return $data = [
                'status' => 403,
                'data' => 'No autorizado'
            ];
        }

        $encuestas = Encuesta::all(['id', 'nombre']);

        return $encuestas;

    }

}
