<?php

namespace App\Http\Controllers;

use App\Models\Encuesta;
use App\models\Pregunta;
use App\models\RespuestaEncuesta;
use App\models\RespuestaPreguntaEncuesta;
use App\services\JwtService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class EncuestaController extends Controller
{
    public function index(Request $request)
    {
        $encuesta = ['status' => 'error', 'msg' => 'No tiene permiso para acceder a esta area'];
        $jwt_auth = new JwtService();

        $token = $request->get('authorization', null);

        $numencuesta = $request->get('encuesta', null);

        $auth_check = $jwt_auth->checktoken($token);

        if ($auth_check === true) {
            if ($numencuesta === null) {
                $encuesta = ['status' => 'success', 'msg' => Encuesta::with(['preguntas'])->get()];
            } else {
                $data = Encuesta::find($numencuesta);
                $encuesta = ['status' => 'succes', 'msg' => $data];
            }
        }

        return $encuesta;
    }

    public function store(Request $request)
    {
        $respuesta = ['status' => 'error', 'msg' => 'Ha ocurrido un problema, intenta nuevamente '];

        /**
         * validamos recibir info requerida del formulario
         */
        $data = json_decode($request->post('form'), true);
        $reglas = ['nombre' => 'required|string|max:150|unique:encuestas'];
        $mensajes = [
            'nombre.required' => 'Debes dar un nombre la encuesta',
            'nombre.string' => 'El nombre de la encuesta debe ser texto',
            'nombre.max' => 'el largo maximo del nombre puede ser :max caracteres',
            'nombre.unique' => 'Ya existe una encuesta con el nombre indicado'
        ];
        $validator = Validator::make($data, $reglas, $mensajes);
        if ($validator->passes()) {

            $jwt_auth = new JwtService();

            $token = $request->post('authorization', null);

            if ($jwt_auth->checktoken($token)) {

                $encuesta = Encuesta::create([
                    'nombre' => $data['nombre']
                ]);

                $respuesta = ['status' => 'success', 'msg' => "La encuesta $encuesta->nombre se creo correctamente"];

            }

        } else {
            //TODO Handle your error
            $respuesta = ['status' => 'error', 'msg' => $validator->errors()->first()];
        }

        return $respuesta;

    }

    public function view(Request $request)
    {

        $jwt_auth = new JwtService();

        $token = $request->post('authorization', null);

        $id = $request->post('id', null);

        $auth_check = $jwt_auth->checktoken($token);

        if ($auth_check === true) {
            if ($id !== null) {
                $encuesta = Encuesta::with(['preguntas'])
                    ->where('encuestas.id', '=', $id)
                    ->get();
                /*                $encuestas = Encuesta::find($id);
                                $encuesta = $encuestas->preguntas()
                                    ->orderBy('pregunta_encuesta.orden', 'desc')
                                    ->get();*/

                /*$encuesta = Encuesta::join('pregunta_encuesta', 'encuestas.id', '=', 'pregunta_encuesta.encuesta_id')
                                    ->join('preguntas', 'preguntas.id', '=', 'pregunta_encuesta.pregunta_id')
                                    ->where('encuestas.id', $id)
                                    ->get();*/
            } else {

                $encuesta = ['status' => 'error', 'msg' => 'No se encontro informacion'];
            }
        } else {
            $encuesta = ['status' => 'error', 'msg' => 'No tiene permiso para acceder a esta area'];
        }

        return $encuesta;
    }

    public function save(Request $request)
    {
        $respuesta = ['status' => 'error', 'msg' => 'Send data via post'];

        $respuestas = $request->post('encuesta', null);
        $idencuesta = $request->post('id', null);
        $cliente = $request->post('cliente', null);

        // comprobamos que data enviada exista.
        if ($respuestas != null && $idencuesta != null && $cliente) {
            $jwt_auth = new JwtService();

            $token = $request->post('authorization', null);

            $auth_check = $jwt_auth->checktoken($token);

            if ($auth_check) {
                //logica si token es valido

                DB::transaction(function () use (&$respuestas, &$idencuesta, &$cliente) {
                    //pasamos las respuestas a objetos de nuevo para ser procesadas
                    //transaccion de todas las respuesta de la encuesta, si una fall todas se hacen rollback y no se inserta
                    $responses = json_decode($respuestas);
                    $encuestado = json_decode($cliente);
                    $infoencuestado = RespuestaEncuesta::create([
                        'nombre' => $encuestado->nombre,
                        'rut' => $encuestado->rut,
                        'email' => $encuestado->email,
                        'telefono' => $encuestado->telefono,
                        'comentario' => $encuestado->comentario,
                        'encuesta_id' => $idencuesta
                    ]);

                    foreach ($responses as $respuesta) {

                        RespuestaPreguntaEncuesta::create([
                            'respuesta_encuesta_id' => $infoencuestado->id,
                            'pregunta_id' => $respuesta->id,
                            'valor' => $respuesta->valor
                        ]);

                    }
                });

                $respuesta = ['status' => 'success', 'msg' => 'Respuestas ingresadas correctamente'];

            } else {

                $respuesta = ['status' => 'error', 'msg' => 'No tiene autorizacion para realizacr esta accion '];
            }

        }


        return $respuesta;

    }

    /**
     * Metodo que recibe token y un id
     * Retorna arrary con preguntas y lista de preguntas asociadas a la encuesta en base al id recibido
     * @param Request $request
     * @return array
     */
    public function edit(Request $request)
    {
        $jwt_auth = new JwtService();

        $token = $request->post('authorization', null);

        if ($jwt_auth->checktoken($token)) {

            $id = $request->post('id', null);

            if ($id !== null) {

                $preguntas = Pregunta::all();
                $encuesta = Pregunta::Join('pregunta_encuesta', 'pregunta_id', 'id')
                    ->Where('encuesta_id', $id)
                    ->Get(['id']);
                $data = ['status' => 'success', 'data' => [
                    'preguntas' => $preguntas,
                    'encuesta' => $encuesta
                ]];

            }

        }
        return $data;
    }

    public function editstore(Request $request, $id)
    {
        $jwt_auth = new JwtService();

        $token = $request->post('authorization', null);

        $data = ['error' => 'success', 'msg' => 'Intente nuevamente'];

        if ($jwt_auth->checktoken($token)) {

            $respuestas = $request->post('encuesta', null);
            $sourceid = [];

            if (null != $respuestas) {
                $obj = json_decode($respuestas);

                foreach ($obj as $respuesta) {
                    if ($respuesta->selected == true) {
                        $sourceid[] = $respuesta->id;
                    }
                }
            }
            //sincronizamos tabla intermedia
            try {
                $encuesta = Encuesta::find($id);
                $encuesta->preguntas()->sync($sourceid);
                $data = ['status' => 'success', 'msg' => 'Se actualizo corrrectamente la encuesta'];
            } catch (\Exception $exception) {
                $mensaje = $exception->getMessage();

            } catch (ModelNotFoundException $modelNotFoundException) {
                $mensaje = $modelNotFoundException->getMessage();

            }


        }

        return $data;
    }
}
