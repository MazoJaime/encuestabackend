<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use App\services\JwtService;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class AuthApiController extends Controller
{
    /**
     * @param LoginRequest $request
     * @return array
     */
    function login(LoginRequest $request)
    {
        $data = [
            'status' => 'error',
            'msg' => 'Send json via post'
        ];

        $email = $request->input('email', null);
        $password = $request->input('password', null);
        $getHash = $request->input('getHash', null);

        if (NULL != $password && NULL != $email) {

            $jwt_auth = new JwtService();

            if ($getHash == null || $getHash == false) {
                $data = $jwt_auth->singup($email, $password);
            } else {
                $data = $jwt_auth->singup($email, $password, true);
            }

        } else {
            $data = [
                'status' => 'error',
                'msg' => 'Email malo o password incorrectos'
            ];
        }

        return $data;
    }

    /**
     * @param Request $request
     * @return array|object|string
     */
    function register(RegisterRequest $request)
    {

        $data = [
            'status' => 'error',
            'msg' => 'No ha sido posible crear su usario'
        ];

        $email = $request->input('email', null);
        $password = $request->input('password', null);
        $nombre = $request->input('name', null);
        $role = $request->input('role', null);


        try {
            $user = new User();
            $user->name = $nombre;
            $user->email = $email;
            $user->setPasswordAttribute($password);
            $user->role_id = $role;
            $user->save();


            $data = [
                'status' => 'success',
                'msg' => 'Usuario creado correctamente',
                'user' => $user
            ];
        } catch (\Exception $e) {
            $data['msg'] = $e->getMessage();
            return $data;
        }

        return $data;
    }


}
