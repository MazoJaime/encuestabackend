<?php

namespace App\Http\Controllers;

use App\models\RespuestaEncuesta;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReporteController extends Controller
{
    public function index(Request $request)
    {

        return RespuestaEncuesta::with(['detallerespuesta', 'encuesta'])->get();


    }
}
