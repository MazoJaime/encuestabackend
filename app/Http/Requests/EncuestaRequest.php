<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EncuestaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|string|max:150'
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' =>'Debes dar un nombre la encuesta',
            'nombre.string' =>'El nombre de la encuesta debe ser texto',
            'nombre.max' => 'el largo maximo del nombre puede ser :max caracteres'
        ];
    }
}
