<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaEncuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_encuesta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 150);
            $table->string('rut', 11);
            $table->string('email', 150)->nullable();
            $table->string('telefono', 15)->nullable();
            $table->text('comentario')->nullable();
            $table->integer('encuesta_id', false, true);
            $table->foreign('encuesta_id')->references('id')->on('encuestas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_encuesta');
    }
}
