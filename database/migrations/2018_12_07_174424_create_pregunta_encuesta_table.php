<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreguntaEncuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pregunta_encuesta', function (Blueprint $table) {
            $table->integer('pregunta_id', false, true);
            $table->integer('encuesta_id', false, true);
            $table->primary(['pregunta_id', 'encuesta_id']);
            $table->foreign('pregunta_id')->references('id')->on('preguntas');
            $table->foreign('encuesta_id')->references('id')->on('encuestas');
            $table->integer('orden', false, true)->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pregunta_encuesta');
    }
}
