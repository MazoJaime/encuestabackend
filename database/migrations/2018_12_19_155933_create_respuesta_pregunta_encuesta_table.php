<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaPreguntaEncuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta_pregunta_encuesta', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('respuesta_encuesta_id', false, true);
            $table->foreign('respuesta_encuesta_id')->references('id')->on('respuesta_encuesta');

            $table->integer('pregunta_id', false, true);
            $table->foreign('pregunta_id')->references('id')->on('preguntas');




            $table->integer('valor', false, true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('respuesta_pregunta_encuesta');
    }
}
